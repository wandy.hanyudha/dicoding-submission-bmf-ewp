btnSearch.addEventListener("click",function(event) {
    event.preventDefault()
    if (localStorage.getItem(STORAGE_KEY) === "") {
        alert("Tidak ada data buku");
        return location.reload();
    } else {
        //Search by Title
        const getByTitle = getData().filter(a => a.title.toLowerCase().includes(searchValue.value.toLowerCase()));
            if (getByTitle.length === 0) {

                //Search by Author
                const getByAuthor = getData().filter(a => a.author.toLowerCase().includes(searchValue.value.toLowerCase()));
                if (getByAuthor.length === 0) {

                    //Search by Tahun
                    const getByYear = getData().filter(a => a.yearReleased === searchValue.value.trim());
                    if (getByYear.length === 0) {
                        alert(`Data yang anda cari tidak ditemukan`);
                    return location.reload();
                }else{
                    showSearchResult(getByYear);
                }
            }else{
                showSearchResult(getByAuthor);
            }
        }else{
            showSearchResult(getByTitle);
        }
    }

    searchValue.value = '';
})

function showSearchResult(books) {
    const searchResult = document.querySelector("#searchResult");

    searchResult.innerHTML = '';

    books.forEach(book => {
        let el = `
        <article class="book_item">
            <h3>${book.title}</h3>
            <p>Penulis: ${book.author}</p>
            <p>Tahun: ${book.yearReleased}</p>
            <p class="ket">Keterangan : <span>${book.isRead ? 'Sudah dibaca' : 'Belum selesai dibaca'}</span></p>
        </article>
        `
        searchResult.innerHTML += el;
    });
}