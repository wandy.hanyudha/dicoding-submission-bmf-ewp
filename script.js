document.addEventListener("DOMContentLoaded", () => {
    const submitForm = document.getElementById("inputBook");
    const checkbox = document.getElementById("isRead");

    submitForm.addEventListener("submit", (event) => {
        event.preventDefault();
        if (checkbox.checked) {
            listSudahDibaca();
        } else {
            listBelumDibaca();
        }
    });
    if (isStorageExist()) {
        loadDataFromStorage();
    }
});

document.addEventListener("ondataloaded", () => {
    refreshData();
});
