const BELUM_DIBACA = "incompleteBookshelfList";
const SUDAH_DIBACA = "completeBookshelfList";
const BOOK_ID = "itemId";
const searchValue = document.querySelector("#searchBookTitle");
const btnSearch = document.querySelector("#searchSubmit");

/**
 * 
 * @param title
 * @param author
 * @param yearReleased
 * @param isComplete
 * @returns {HTMLElement}
 */
const dataBuku = (title, author, yearReleased, isComplete) => {
    const textTitle = document.createElement("h3");
    textTitle.innerText = title;

    const textAuthor = document.createElement("p");
    textAuthor.innerText = author;

    const textYear = document.createElement("p");
    textYear.classList.add("year");
    textYear.innerText = yearReleased;

    const container = document.createElement("article");
    container.classList.add("book_item");
    container.append(textTitle, textAuthor, textYear);
    if (isComplete){
        container.append(createUnreadButton(), createDeleteButton());
    } else {
        container.append(createReadButton(), createDeleteButton());
    }

    return container;
}

//Blueprint Button
/**
 * 
 * @param textButton
 * @param buttonClass
 * @param eventListener
 * @returns {HTMLButtonElement}
 */
const createButton = (textButton, buttonClass, eventListener) => {
    const button = document.createElement("button");
    button.innerText = textButton;
    button.classList.add(buttonClass);
    button.addEventListener("click", (event) => {
        eventListener(event);
    });
    return button;
};

//button checklist buku sudah dibaca
const createReadButton = () => {
    return createButton("Selesai dibaca", "green", (event) => {
        changeToReadList(event.target.parentElement);
    })
};

//button untuk membatalkan buku yg sudah dibaca
//mengubah isRead = true menjadi bernilai false
const createUnreadButton = () => {
    return createButton("Belum selesai di Baca", "green", (event) => {
        changeToUnreadList(event.target.parentElement);
    });
};

//button untuk menghapus buku dari list
const createDeleteButton = () => {
    return createButton("Hapus Buku", "red", (event) => {
        var result = confirm("Apakah anda yakin ingin menghapus buku ini?")
        if (result){
        removeBookList(event.target.parentElement);
        }
    });
};


const listBelumDibaca = () => {
    const belumDibaca = document.getElementById(BELUM_DIBACA);
    const titleBook = document.getElementById("title").value;
    const authorBook = document.getElementById("author").value;
    const yearBook = document.getElementById("year").value;
    const book = dataBuku(titleBook, authorBook, yearBook, false);
    const bookObject = composeBookObject(titleBook, authorBook, yearBook, false);
    book[BOOK_ID] = bookObject.id;
    booksData.push(bookObject);
    belumDibaca.append(book);
    updateDataToStorage();
}

const listSudahDibaca = () => {
    const bookHasBeenRead = document.getElementById(SUDAH_DIBACA);
    const titleBook = document.getElementById("title").value;
    const authorBook = document.getElementById("author").value;
    const yearBook = document.getElementById("year").value;
    const book = dataBuku(titleBook, authorBook, yearBook, true);
    const bookObject = composeBookObject(titleBook, authorBook, yearBook, true);
    book[BOOK_ID] = bookObject.id;
    booksData.push(bookObject);
    bookHasBeenRead.append(book);
    updateDataToStorage();
}

/**
 *
 * @param booksElement
 */
const changeToReadList = (booksElement) => {
    const bookHasBeenRead = document.getElementById([SUDAH_DIBACA]);
    const titleBook = booksElement.querySelector(".book_item > h3").innerText;
    const authorBook = booksElement.querySelector(".book_item > p").innerText;
    const yearBook = booksElement.querySelector(".book_item > p.year").innerText;
    const newBook = dataBuku(titleBook, authorBook, yearBook, true);
    const book = findBook(booksElement[BOOK_ID]);

    book.isRead = true;
    newBook[BOOK_ID] = book.id;
    bookHasBeenRead.append(newBook);
    booksElement.remove();
    updateDataToStorage();
};

const changeToUnreadList = (booksElement) => {
    const bookIsntReadYet = document.getElementById([BELUM_DIBACA]);
    const titleBook = booksElement.querySelector(".book_item > h3").innerText;
    const authorBook = booksElement.querySelector(".book_item > p").innerText;
    const yearBook = booksElement.querySelector(".book_item > p.year").innerText;
    const newBook = dataBuku(titleBook, authorBook, yearBook, false);
    const book = findBook(booksElement[BOOK_ID]);
    book.isRead = false;
    newBook[BOOK_ID] = book.id;
    bookIsntReadYet.append(newBook);
    booksElement.remove();
    updateDataToStorage();
}


const removeBookList = (booksElement) => {
    const bookPosition = findBookIndex(booksElement[BOOK_ID]);
    booksData.splice(bookPosition, 1);
    booksElement.remove();
    updateDataToStorage();
};

const getData = () => {
    return JSON.parse(localStorage.getItem(STORAGE_KEY)) || [];
}

const refreshData = () => {
    const belumDibaca = document.getElementById(BELUM_DIBACA);
    const sudahDibaca = document.getElementById(SUDAH_DIBACA);

    for (book of booksData) {
        const newBook = dataBuku(book.title, book.author, book.yearReleased, book.isRead);
        newBook[BOOK_ID] = book.id;

        if (book.isRead){
            sudahDibaca.append(newBook);
        } else {
            belumDibaca.append(newBook)
        }
    }
};